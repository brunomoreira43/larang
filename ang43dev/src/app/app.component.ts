import { Component } from '@angular/core';
import { Item } from './item';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response } from '@angular/http';
import { Injectable } from '@angular/core';
//import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'Ang43d.EV';

  constructor(private _http: Http){}
  private headers = new Headers({'Content-Type': 'application/json'});
  onSubmit(form: NgForm): Promise <Item> {
    console.log(form.value);
      return this._http.post('http://127.0.0.1:8000/api/items', JSON.stringify(form.value), {headers: this.headers})
        .toPromise()
        .then(res => res.json().data)
          .catch(this.handleError);
    }
  private handleError(error:any): Promise<any>{
    console.error('An error has occurred', error);
    return Promise.reject(error.message || error);
  }
  
}
