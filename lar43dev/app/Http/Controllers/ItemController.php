<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Item;

class ItemController extends Controller
{
    /** 
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    *
    **/ 
    public function index() {
        return view('api');
    }

    // function responsible for showing /api/view. And it is not in a separeted controller for testing proupurses... 

    public function view(){
        $items = DB::table('items')->get();

        return view('view', ['items' => $items]);
    }
    public function store(Request $request) {

        $item = new Item ([
            'name'  => $request->get('name'),
            'price' => $request->get('price')
        ]);
        $item->save();
        return response()->json('Added'); 


    }

}
